#Encoding: Cp1252
@Servico  @firefox 
Feature: Cadastro de Servi�o 

Background: 
	Given que estou no navegador "firefox" 
	And acesso o sistema 
	And n�o possuo registros cadastrados no sistema 

@1_servico
Scenario: 01 - Cadastro de Servi�o 
	Given eu estou na tela de "Bem Vindo!" 
	When eu clico no menu "servicos" 
	Then eu estou na tela "Servi�os" 
	When eu clico no bot�o "Adicionar" 
	Then eu estou no modal "Servi�o" 
	When eu preencho os campos de cadastro de Servi�o 
		|Descri��o		   	  |Valor|
		|Instala��o Windows   |50.0 | 
	And eu clico no bot�o "Salvar" 
	Then eu verifico a mensagem "Servi�o cadastrado com sucesso!" 
	
@2_servico
Scenario: 02 - Cadastrar servi�o com campos obrigat�rios vazios 
	Given eu estou na tela de "Bem Vindo!" 
	When eu clico no menu "servicos" 
	Then eu estou na tela "Servi�os"
	When eu clico no bot�o "Adicionar" 
	Then eu estou no modal "Servi�o" 
	When eu preencho os campos de cadastro de Servi�o 
		|Descri��o	|Valor|
		|   		|     | 
	And eu clico no bot�o "Salvar" 
	Then eu verifico a mensagem de erro no modal "Por favor verifique os erros do formul�rio" 

@3_servico	 
Scenario: Excluir Servi�o com sucesso. 
	Given possuo o seguinte registro de servi�o cadastrado
		|Descri��o			|Valor |
		|Instala��o Windows |50.00 | 
	And eu estou na tela de "Bem Vindo!" 
	When eu clico no menu "servicos" 
	Then eu estou na tela "Servi�os"
	When eu clico no servi�o "Instala��o Windows"
		And eu clico no bot�o excluir servico
		And eu clico no bot�o confirmar
	Then eu verifico a mensagem "Servi�o exclu�do com sucesso!" 

@4_servico		
Scenario: Atualizar Servi�o com sucesso. 
	Given possuo o seguinte registro de servi�o cadastrado
		|Descri��o			|Valor |
		|Instala��o Windows |50.00 | 
	And eu estou na tela de "Bem Vindo!" 
	When eu clico no menu "servicos" 
	Then eu estou na tela "Servi�os" 
	When eu clico no servi�o "Instala��o Windows"
	Then eu estou no modal "Servi�o" 
	When eu preencho os campos de cadastro de Servi�o 
		|Descri��o		  |Valor |
		|Instala��o Linux |30.00 | 
	And eu clico no bot�o "Salvar"
	Then eu verifico a mensagem "Servi�o cadastrado com updated"
		And eu verifico o resultado na tabela de servi�o
		|Descri��o		  |Valor |
		|Instala��o Linux |30.00 | 

@5_servico		
Scenario: Alterar servi�o e salvar com campos obrigat�rios vazios
	Given possuo o seguinte registro de servi�o cadastrado
		|Descri��o			|Valor |
		|Instala��o Windows |50.00 |  
	And eu estou na tela de "Bem Vindo!" 
	When eu clico no menu "servicos" 
	Then eu estou na tela "Servi�os"
	When eu clico no servi�o "Instala��o Windows"
	Then eu estou no modal "Servi�o"  
	When eu preencho os campos de cadastro de Servi�o 
		|Descri��o	|Valor |
		| 			|	   | 
	And eu clico no bot�o "Salvar"
	Then eu verifico a mensagem de erro no modal "Por favor verifique os erros do formul�rio" 

@6_servico		
Scenario: Verificar limites superiores e inferiores dos campos de cadastro de servi�o. 
	Given eu estou na tela de "Bem Vindo!" 
	When eu clico no menu "servicos" 
	Then eu estou na tela "Servi�os"
		When eu clico no bot�o "Adicionar" 
	Then eu estou no modal "Servi�o" 
	When eu preencho os campos de cadastro de Servi�o 
		|Descri��o		   	  																					 |Valor|
		|01234567890123456718901234567901234567890123456789012345678901234567890123456789012345678901234567891   |50.0 | 
	And eu clico no bot�o "Salvar"
	Then eu verifico a mensagem de erro no modal "Por favor verifique os erros do formul�rio" 
	And eu verifico a mensagem de erro no modal para o campo descri��o "Descricao teve seu valor excedido de 100 caracteres."  