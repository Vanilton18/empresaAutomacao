#Encoding: Cp1252
@Recibo @safari
Feature: Cadastro de Recibo 

Background: 
	Given que estou no navegador "Safari" 
	And acesso o sistema 
	And n�o possuo registros cadastrados no sistema 
	Then eu estou na tela de "Bem Vindo!" 
		
@1_recibo
Scenario: 01 - Cadastro de Recibo
	Given possuo o seguinte cliente registrado no sistema 
		|Cliente |Email				  |Endere�o					   |Hist�rico|
		|Vanilton|vanilton18@gmail.com|Av.Camapu� N� 442 C. Nova II|N/A      |
		And possuo o seguinte servi�o registrado no sistema 
		|Descri��o		   |Valor|
		|Formata��o Windows|50.0 | 
	When eu clico no menu "recibos" 
	Then eu estou na tela "Recibos" 
	When eu clico no bot�o "Adicionar" 
	Then eu estou no modal "Recibo" 
	When eu preencho os campos do modal Recibo 
		|Data	   |Servi�o			   |Cliente |
		|01-01-2015|Formata��o Windows |Vanilton| 
	And eu clico no bot�o "Salvar" 
	Then eu verifico a mensagem "Recibo cadastrado com sucesso!"

@2_recibo	
Scenario: 02 - Excluir Recibo com sucesso.
	Given possuo o seguinte cliente registrado no sistema com recibo
		|Cliente |Email				  |Endere�o					   |Hist�rico|Descri��o			|Valor|Data Recibo|
		|Vanilton|vanilton18@gmail.com|Av.Camapu� N� 442 C. Nova II|N/A      |Formata��o Windows|50.00|2015-11-15 | 
	When eu clico no menu "recibos" 
	Then eu estou na tela "Recibos"
	When eu clico no recibo "15-11-2015"
		And eu clico no bot�o excluir recibo
		And eu clico no bot�o confirmar
	Then eu verifico a mensagem "Recibo exclu�do com sucesso!"  

@3_recibo	
Scenario: 03 - Atualizar Recibo com sucesso. 
	Given possuo o seguinte cliente registrado no sistema com recibo
		|Cliente |Email				  |Endere�o					   |Hist�rico|Descri��o			|Valor|Data Recibo|
		|Vanilton|vanilton18@gmail.com|Av.Camapu� N� 442 C. Nova II|N/A      |Formata��o Windows|50.00|2015-11-15 | 
	When eu clico no menu "recibos" 
	Then eu estou na tela "Recibos"
	When eu clico no recibo "15-11-2015"
	Then eu estou no modal "Recibo" 
	When eu preencho o campo data com "01-01-2016"
		And eu clico no bot�o "Salvar"
	Then eu verifico a mensagem "Recibo atualizado com sucesso!"  
		And eu verifico o recibo na tabela "01-01-2016"

@4_recibo	
Scenario: 04 - Cadastrar recibo com campos obrigat�rios vazios 
	When eu clico no menu "recibos" 
	Then eu estou na tela "Recibos" 
	When eu clico no bot�o "Adicionar" 
	Then eu estou no modal "Recibo" 
	When eu clico no bot�o "Salvar" 
	Then eu verifico a mensagem de erro no modal "Por favor verifique os erros do formul�rio" 
	
@5_recibo	
Scenario: 05 - Alterar recibo e salvar com campos obrigat�rios vazios 
	Given possuo o seguinte cliente registrado no sistema com recibo
		|Cliente |Email				  |Endere�o					   |Hist�rico|Descri��o			|Valor|Data Recibo|
		|Vanilton|vanilton18@gmail.com|Av.Camapu� N� 442 C. Nova II|N/A      |Formata��o Windows|50.00|2015-11-15 | 
	When eu clico no menu "recibos" 
	Then eu estou na tela "Recibos" 
	When eu clico no recibo "15-11-2015"
	Then eu estou no modal "Recibo" 
	And eu clico no bot�o remover servi�o
	And eu clico no bot�o remover cliente
	And eu clico no bot�o "Salvar"
	Then eu verifico a mensagem de erro no modal "Por favor verifique os erros do formul�rio"  