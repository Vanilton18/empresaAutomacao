#Encoding: Cp1252
@Cliente @safari
Feature: Cadastro de Cliente 

Background: 
	Given que estou no navegador "Safari" 
		And acesso o sistema 
		And n�o possuo registros cadastrados no sistema
		And eu estou na tela de "Bem Vindo!"

@1_cliente 
Scenario: 01 - Cadastro de cliente
	When eu clico no menu "clientes" 
	Then eu estou na tela "Gerenciar Clientes"
	When eu clico no bot�o "Adicionar" 
	Then eu estou no modal "Cliente" 
	When eu preencho os campos do modal Manter Cliente 
		|Nome		   	  |Email			   |Endere�o		|Hist�rico	|
		|Vanilton Pinheiro|vanilto18@gmail.com |Av. Camapu� 442 |N/A		| 
	And eu clico no bot�o "Salvar"  
	Then eu verifico a mensagem "Cliente cadastrado com sucesso!" 

@2_cliente	
Scenario: 02 - Cadastar cliente com campos obrigat�rios vazios.
	When eu clico no menu "clientes" 
	Then eu estou na tela "Gerenciar Clientes"
	When eu clico no bot�o "Adicionar" 
	Then eu estou no modal "Cliente" 
	When eu preencho os campos do modal Manter Cliente 
		|Nome	|Email	|Endere�o |Hist�rico|
		|		|		| 		  |			| 
	And eu clico no bot�o "Salvar"  
	Then eu verifico a mensagem de erro no modal "Por favor verifique os erros do formul�rio"

@3_cliente	
Scenario: 03 - Excluir cliente com sucesso.
	Given possuo o seguinte cliente cadastrado no sistema
		|Nome			  |Email			   |Endere�o		|Hist�rico|
		|Vanilton Pinheiro|vanilto18@gmail.com |Av. Camapu� 442 |N/A	  | 
	When eu clico no menu "clientes" 
	Then eu estou na tela "Gerenciar Clientes"
	When eu clico no cliente "Vanilton Pinheiro"
		And eu clico no bot�o excluir cliente
		And eu clico no bot�o confirmar
	Then eu verifico a mensagem "Cliente exclu�do com sucesso!" 
	 
@4_cliente
Scenario: 04 - Atualizar cliente com sucesso.
	Given possuo o seguinte cliente cadastrado no sistema
		|Nome			  |Email			   |Endere�o		|Hist�rico|
		|Vanilton Pinheiro|vanilto18@gmail.com |Av. Camapu� 442 |N/A	  | 
	When eu clico no menu "clientes" 
	Then eu estou na tela "Gerenciar Clientes"
	When eu clico no cliente "Vanilton Pinheiro"
	Then eu estou no modal "Cliente" 
	When eu preencho os campos do modal Manter Cliente 
		|Nome		   	  |Email			 |Endere�o		|Hist�rico	|
		|Vanilton		  |vanilto@gmail.com |Av. Camapu� 60|Test		| 
	And eu clico no bot�o "Salvar"
	Then eu verifico a mensagem "Cliente atualizado com sucesso!"
		And eu verifico o resultado na tabela de cliente
		|Nome		   	  |Email			 |Endere�o		|Hist�rico	|
		|Vanilton		  |vanilto@gmail.com |Av. Camapu� 60|Test		| 

@5_cliente
Scenario: 05 - Deletar cliente que possui um recibo vinculado
	Given possuo o seguinte cliente registrado no sistema com recibo
		|Cliente |Email				  |Endere�o					   |Hist�rico|Descri��o			|Valor|Data Recibo|
		|Vanilton|vanilton18@gmail.com|Av.Camapu� N� 442 C. Nova II|N/A      |Formata��o Windows|50.00|2015-11-15 |
	When eu clico no menu "clientes" 
	Then eu estou na tela "Gerenciar Clientes" 
	When eu clico no cliente "Vanilton"
		And eu clico no bot�o excluir cliente
		And eu clico no bot�o confirmar
	Then eu verifico a mensagem de erro no modal "Cliente possui recibos vinculados" 

@6_cliente
Scenario: 06 - Alterar cliente e salvar com campos obrigat�rios vazios.
	Given possuo o seguinte cliente cadastrado no sistema
		|Nome			  |Email			   |Endere�o		|Hist�rico|
		|Vanilton Pinheiro|vanilto18@gmail.com |Av. Camapu� 442 |N/A	  | 
	When eu clico no menu "clientes" 
	Then eu estou na tela "Gerenciar Clientes"
	When eu clico no cliente "Vanilton Pinheiro"
	Then eu estou no modal "Cliente" 
	When eu preencho os campos do modal Manter Cliente 
		|Nome 	  |Email |Endere�o	|Hist�rico	|
		|		  |		 |			|			| 
	And eu clico no bot�o "Salvar"
	Then eu verifico a mensagem de erro no modal "Por favor verifique os erros do formul�rio"

@7_cliente
Scenario: 07 - Verificar limites superiores e inferiores dos campos de cadastro de cliente.
	When eu clico no menu "clientes" 
	Then eu estou na tela "Gerenciar Clientes"
	When eu clico no bot�o "Adicionar" 
	Then eu estou no modal "Cliente" 
	When eu preencho os campos do modal Manter Cliente 
		|Nome		   	  									|Email			    |Endere�o		|Hist�rico	|
		|a01234567890123456789012345678901234567890123456789|vanilto18@gmail.com|Av. Camapu� 442|N/A		| 
	And eu clico no bot�o "Salvar"  
	Then eu verifico a mensagem de erro no modal "Por favor verifique os erros do formul�rio"