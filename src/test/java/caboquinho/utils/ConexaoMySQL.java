package caboquinho.utils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexaoMySQL {

	private static Connection connection;

	public static Connection startConnection() throws SQLException,
			IOException, ClassNotFoundException {
		Connection connection = null;

		String txServerBdName = Util.getProps("host");
		int nbConnectionPort = Integer.valueOf(Util.getProps("port"));
		String txBdName = Util.getProps("database");
		String txUser = Util.getProps("user");
		String txPassword = Util.getProps("key");
		

		if (connection == null || connection.isClosed()) {
			String url = "jdbc:Mysql://" + txServerBdName + ":"
					+ nbConnectionPort + "/" + txBdName;
			String driver = "com.mysql.jdbc.Driver";
			try {
				Class.forName(driver).newInstance();
				connection = DriverManager.getConnection(url, txUser,
						txPassword);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return connection;
	}

	public static void closeConnection() throws SQLException {
		if (connection != null && !connection.isClosed())
			connection.close();
	}
}
