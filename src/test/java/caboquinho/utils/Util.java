package caboquinho.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import caboquinho.glue.WebDriverCreator;

public class Util extends WebDriverCreator {
	public static final String DADOS = "propriedades.properties";

	public static String getProps(String chave) throws IOException {
		Properties prop = new Properties();
		FileInputStream file = new FileInputStream(DADOS);
		prop.load(file);
		return prop.getProperty(chave);

	}

	public static String finalizaProcesso(String nomeProcesso)
			throws IOException {
		try {
			Runtime.getRuntime().exec(
					"cmd /c taskkill /f /IM " + nomeProcesso.concat(".exe")
							+ " /t");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Processo " + nomeProcesso + " finalizado!";
	}

	public static boolean isPresent(By localizador, WebDriver driver) {
		boolean present;
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		present = driver.findElements(localizador).size() != 0;
		driver.manage().timeouts().implicitlyWait(1500, TimeUnit.SECONDS);
		return present;
	}
	
	public static void fecharBrowser(){
		driver.quit();
	}
}
