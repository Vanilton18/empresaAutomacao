package caboquinho.utils;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class ScriptDatabase {

	public static void LimparBase() throws ClassNotFoundException, SQLException, IOException {

		String sqlSafeOff = "SET SQL_SAFE_UPDATES=0;";

		String sqlDelRecibo = "DELETE recibo.* FROM recibo;";
		String sqlDelServico = "DELETE servico.* FROM servico;";
		String sqlDelCliente = "DELETE cliente.* FROM cliente;";
		PreparedStatement stmSafeOff = ConexaoMySQL.startConnection().prepareStatement(sqlSafeOff);
		PreparedStatement stmDelRecibo = ConexaoMySQL.startConnection().prepareStatement(sqlDelRecibo);
		PreparedStatement stmDelServico = ConexaoMySQL.startConnection().prepareStatement(sqlDelServico);
		PreparedStatement stmDelCliente = ConexaoMySQL.startConnection().prepareStatement(sqlDelCliente);

		try {
			stmSafeOff.execute();
			stmDelRecibo.execute();
			stmDelServico.execute();
			stmDelCliente.execute();
			stmSafeOff.close();
			;
			stmDelRecibo.close();
			stmDelServico.close();
			stmDelCliente.close();
			System.out.println("Base limpa com sucesso!");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void inserirClienteNaBase(String cliente, String email, String endereco, String historico)
			throws ClassNotFoundException, SQLException, IOException {

		String sqlInsertCliente = "INSERT INTO cliente (`nome`,`email`,`endereco`,`historico`) VALUES(?,?,?,?);";

		PreparedStatement stmInserirCliente = ConexaoMySQL.startConnection().prepareStatement(sqlInsertCliente);
		stmInserirCliente.setString(1, cliente);
		stmInserirCliente.setString(2, email);
		stmInserirCliente.setString(3, endereco);
		stmInserirCliente.setString(4, historico);

		try {
			stmInserirCliente.execute();

			stmInserirCliente.close();

			System.out.println("Cliente " + cliente + " cadastrado na base com sucesso!");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void inserirServicoNaBase(String descricao, String valor)
			throws ClassNotFoundException, SQLException, IOException {
		String sqlInsertServico = "INSERT INTO servico (`descricao`,`valor`) VALUES(?,?);";

		PreparedStatement stmInserirServico = ConexaoMySQL.startConnection().prepareStatement(sqlInsertServico);
		stmInserirServico.setString(1, descricao);
		stmInserirServico.setString(2, valor);

		try {
			stmInserirServico.execute();

			stmInserirServico.close();

			System.out.println("Servi�o " + descricao + " cadastrado na base com sucesso!");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static void inserirReciboNaBase(String cliente, String email, String endereco, String historico,
			String descricao, String valor, String dtRecibo) throws ClassNotFoundException, SQLException, IOException {

		String sqlInsertCliente = "INSERT INTO cliente (`nome`,`email`,`endereco`,`historico`) VALUES('" + cliente + "','"
				+ email + "','" + endereco + "','" + historico + "');";
		String sqlInsertServico = "INSERT INTO servico (`descricao`,`valor`) VALUES('" + descricao + "','" + valor + "');";
		String sqlInsertRecibo = "INSERT INTO recibo(`data`,`servico_id`,`cliente_id`) VALUES ('" + dtRecibo
				+ "',(SELECT id FROM servico where descricao = '" + descricao
				+ "') ,(SELECT id FROM cliente where nome = '" + cliente + "'));";

		Statement smt = ConexaoMySQL.startConnection().createStatement();

		try {
			smt.execute(sqlInsertCliente);
			smt.execute(sqlInsertServico);
			smt.execute(sqlInsertRecibo);
			smt.close();
			System.out.println("Recibo de " + dtRecibo + " cadastrado na base com sucesso!");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
