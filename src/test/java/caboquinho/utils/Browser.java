package caboquinho.utils;

public enum Browser {

	FIREFOX, CHROME, SAFARI, IE, OPERA, EDGE, CHROME_ANDROID;
}
