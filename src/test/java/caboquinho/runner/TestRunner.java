package caboquinho.runner;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.AfterClass;
import org.junit.runner.RunWith;

import caboquinho.utils.Util;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		plugin = {"html:target/cucumber/report", 
		  "json:target/cucumber.json"},
		features = {"src/test/resources/browser/firefox"},
		glue="caboquinho.glue",
		tags="@Cliente")

public class TestRunner {
	
	@AfterClass
	public static void FinalizaTeste() 
			throws IOException, ClassNotFoundException, SQLException {
		Util.fecharBrowser();
		Util.finalizaProcesso("chromedriver");
		Util.finalizaProcesso("operadriver");
		Util.finalizaProcesso("IEDriverServer");
	}

} 
