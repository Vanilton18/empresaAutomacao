package caboquinho.glue;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import caboquinho.utils.Browser;
import caboquinho.utils.Util;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;

public class BrowsersInstances extends WebDriverCreator {

	@After
	public void finalizar(Scenario scenario) throws IOException {
		if (scenario.isFailed()) {
			byte[] screenshot = ((RemoteWebDriver) driver).getScreenshotAs(OutputType.BYTES);
			scenario.embed(screenshot, "image/png");
		}
		if(capacidade != null){
			capacidade.setCapability("resetKeyboard", true);	
		}
		driver.navigate().to(Util.getProps("url"));

	}

	public void initManage() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
	}

	@SuppressWarnings("static-access")
	@Given("^que estou no navegador \"(.*?)\"$")
	public void que_estou_no_navegador(String navegador) throws Throwable {
		String ParseNavegador = navegador.toUpperCase();
		DesiredCapabilities cap;
		Long timeout = Long.valueOf(Util.getProps("timeoutDefault"));
		if (getDriver() == null) {
			switch (Browser.valueOf(ParseNavegador)) {
			case CHROME:
				cap = new DesiredCapabilities().chrome();
				driver = new ChromeDriver(cap);
				action= new Actions(driver);
				wait = new WebDriverWait(driver, 20);
				this.initManage();
				break;
			case CHROME_ANDROID:
				service = AppiumDriverLocalService
						.buildService(new AppiumServiceBuilder().withArgument(GeneralServerFlag.SESSION_OVERRIDE));
				service.start();
				capacidade = new DesiredCapabilities();

				capacidade.setCapability(MobileCapabilityType.DEVICE_NAME,"ANDROID");
			    capacidade.setCapability(MobileCapabilityType.PLATFORM, "ANDROID");
			    capacidade.setCapability(MobileCapabilityType.BROWSER_NAME, "Chrome");
			    capacidade.setCapability(MobileCapabilityType.APP_PACKAGE, "com.android.chrome");
			    capacidade.setCapability(MobileCapabilityType.APP_ACTIVITY, "com.google.android.apps.chrome.Main");
			    capacidade.setCapability("unicodeKeyboard", true);
			    ChromeOptions options = new ChromeOptions();
			    options.addArguments("disable-translate");
			    capacidade.setCapability(ChromeOptions.CAPABILITY, options);
				
			    capacidade.setCapability("chrome.switches", Arrays.asList("--disable-extensions"));
				// abre a aplica��o, informando a conex�o com o servidor do
				// appium
				driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), capacidade);
				action= new Actions(driver);
				wait = new WebDriverWait(driver, timeout);
				break;
			case FIREFOX:
				driver = new FirefoxDriver();
				action= new Actions(driver);
				wait = new WebDriverWait(driver, timeout);
				this.initManage();
				break;
			case IE:
				System.setProperty("webdriver.ie.driver", "drivers//IEDriverServer.exe");
				cap = new DesiredCapabilities().internetExplorer();
				cap.setCapability("ignoreZoomSetting", true);
				cap.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
				cap.setJavascriptEnabled(true);
				driver = new InternetExplorerDriver(cap);
				action= new Actions(driver);
				wait = new WebDriverWait(driver, timeout);
				this.initManage();
				break;
			case OPERA:
				String operaChromiumDriver = "drivers//operadriver.exe";
				System.setProperty("webdriver.chrome.driver", operaChromiumDriver);
				driver = new ChromeDriver();
				action= new Actions(driver);
				wait = new WebDriverWait(driver, timeout);
				this.initManage();
				break;
			case SAFARI:
				driver = new SafariDriver();
				action= new Actions(driver);
				wait = new WebDriverWait(driver, timeout);
				this.initManage();
				break;
			case EDGE:
				System.setProperty("webdriver.edge.driver", "drivers//MicrosoftWebDriver.exe");
				cap = new DesiredCapabilities().edge();
				cap.setCapability("ignoreZoomSetting", true);
				cap.isJavascriptEnabled();
				cap.setCapability("nativeEvents", true);
				driver = new EdgeDriver(cap);
				action= new Actions(driver);
				wait = new WebDriverWait(driver, timeout);
				this.initManage();
				break;
			default:
				break;
			}
		}
	}

	@Given("^acesso o sistema$")
	public void acesso_o_sistema() throws Throwable {
		driver.get(Util.getProps("url"));
	}

}
