package caboquinho.glue;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import caboquinho.utils.ScriptDatabase;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ServiceSteps extends WebDriverCreator {

	@Given("^possuo o seguinte servi�o registrado no sistema$")
	public void possuo_o_seguinte_servi�o_registrado_no_sistema(DataTable table) throws Throwable {
		for (Map<String, String> linha : table.asMaps(String.class, String.class)) {
			String descricao = linha.get("Descri��o");
			String valor = linha.get("Valor");
			ScriptDatabase.inserirServicoNaBase(descricao, valor);
		}
	}

	@When("^eu preencho os campos de cadastro de Servi�o$")
	public void eu_preencho_os_campos_de_cadastro_de_Servi�o(DataTable table) throws Throwable {

		for (List<String> value : table.cells(1)) {
			driver.findElement(By.id("descricao")).clear();
			driver.findElement(By.id("valor")).clear();
			driver.findElement(By.id("descricao")).sendKeys(value.get(0));
			driver.findElement(By.id("valor")).sendKeys(value.get(1));
		}

	}

	@When("^eu preencho o campo descri��o com \"(.*?)\"$")
	public void eu_preencho_o_campo_descri��o_com(String descricao) throws Throwable {
		driver.findElement(By.id("descricao")).sendKeys(descricao);
	}

	@When("^eu preencho o campo valor com \"(.*?)\"$")
	public void eu_preencho_o_campo_valor_com(String valor) throws Throwable {
		driver.findElement(By.id("valor")).sendKeys(valor);
	}

	@Then("^eu verifico a mensagem de erro no modal \"(.*?)\"$")
	public void eu_verifico_a_mensagem_de_erro_no_modal(String txMsgErro) throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='alert alert-error']")));
		assertTrue("Mensagem de erro incorreta",driver.findElement(By.xpath("//div[@class='alert alert-error']")).getText().contains(txMsgErro));
	}

	@Given("^possuo o seguinte registro de servi�o cadastrado$")
	public void possuo_o_seguinte_registro_de_servi�o_cadastrado(DataTable table) throws Throwable {
		String descricao = null;
		String valor = null;

		for (Map<String, String> linha : table.asMaps(String.class, String.class)) {
			descricao = linha.get("Descri��o");
			valor = linha.get("Valor");
		}

		ScriptDatabase.inserirServicoNaBase(descricao, valor);
	}

	@When("^eu clico no servi�o \"(.*?)\"$")
	public void eu_clico_no_servi�o(String descricaoServico) throws Throwable {
		driver.findElement(By.xpath("//td[text()='" + descricaoServico + "']")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("servicoModelContainer")));
	}

	@When("^eu clico no bot�o excluir servico$")
	public void eu_clico_no_bot�o_excluir_servico() throws Throwable {
		driver.findElement(By.id("bt_excluir_servico")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("bt_confirmar_excluir")));
	}

	@When("^eu clico no bot�o confirmar$")
	public void eu_clico_no_bot�o_confirmar() throws Throwable {
		driver.findElement(By.id("bt_confirmar_excluir")).click();
	}

	@Then("^eu verifico o resultado na tabela de servi�o$")
	public void eu_verifico_o_resultado_na_tabela_de_servi�o(DataTable table) throws Throwable {
		String descricao = driver.findElement(By.xpath("//td[1]")).getText();
		String valor = driver.findElement(By.xpath("//td[2]")).getText();

		for (Map<String, String> linha : table.asMaps(String.class, String.class)) {
			assertEquals(linha.get("Descri��o"), descricao);
			assertEquals(linha.get("Valor"), valor);
		}

	}

	@Then("^eu verifico a mensagem de erro no modal para o campo descri��o \"(.*?)\"$")
	public void eu_verifico_a_mensagem_de_erro_no_modal_para_o_campo_descri��o(String msg) throws Throwable {
		assertTrue(driver.findElement(By.xpath("//span[@class='help-inline' and text()='" + msg + "']")).isDisplayed());
	}

}
