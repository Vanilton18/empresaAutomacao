package caboquinho.glue;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

import caboquinho.utils.ScriptDatabase;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ReciboSteps extends WebDriverCreator {

	@When("^eu preencho os campos do modal Recibo$")
	public void eu_preencho_os_campos_do_modal_Recibo(DataTable table) throws Throwable {
		for (Map<String, String> linha : table.asMaps(String.class, String.class)) {
			String servico = linha.get("Servi�o");
			String cliente = linha.get("Cliente");
			
			Actions action = new Actions(driver);
			
			driver.findElement(By.id("data")).clear();
			driver.findElement(By.id("data")).sendKeys(linha.get("Data"));
			driver.findElements(By.cssSelector("input[type='text']")).get(2).sendKeys(servico);
			
			By byServico = By.xpath("//li[contains(@data-value,'"+servico+"')]");
			By byCliente = By.xpath("//li[contains(@data-value,'"+cliente+"')]");
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(byServico));
			action.moveToElement(driver.findElement(byServico)).build().perform();
			action.click().build().perform();
			
			driver.findElements(By.cssSelector("input[type='text']")).get(3).sendKeys(cliente);
			wait.until(ExpectedConditions.visibilityOfElementLocated(byCliente));
			action.moveToElement(driver.findElement(byCliente)).build().perform();
			action.click().build().perform();
			
		}

	}

	@Given("^possuo o seguinte cliente registrado no sistema com recibo$")
	public void possuo_o_seguinte_cliente_registrado_no_sistema_com_recibo(DataTable table) throws Throwable {

		for (Map<String, String> linha : table.asMaps(String.class, String.class)) {
			String cliente = linha.get("Cliente");
			String email = linha.get("Email");
			String endereco = linha.get("Endere�o");
			String historico = linha.get("Hist�rico");
			String descricao = linha.get("Descri��o");
			String valor = linha.get("Valor");
			String dtRecibo = linha.get("Data Recibo");
			ScriptDatabase.inserirReciboNaBase(cliente, email, endereco, historico, descricao, valor, dtRecibo);
		}

	}

	@When("^eu clico no recibo \"(.*?)\"$")
	public void eu_clico_no_recibo(String recibo) throws Throwable {
		driver.findElement(By.xpath("//td[text()='" + recibo + "']")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("reciboDetailDialog")));

	}

	@When("^eu clico no bot�o excluir recibo$")
	public void eu_clico_no_bot�o_excluir_recibo() throws Throwable {
		driver.findElement(By.id("bt_excluir_recibo")).click();
	}

	@When("^eu preencho o campo data com \"(.*?)\"$")
	public void eu_preencho_o_campo_data_com(String data) throws Throwable {
		driver.findElement(By.id("data")).clear();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("$('#data').datepicker('setDate', new Date('" + data + "'));");
	}

	@Then("^eu verifico o recibo na tabela \"(.*?)\"$")
	public void eu_verifico_o_recibo_na_tabela(String dataAlterada) throws Throwable {
		String data = driver.findElement(By.xpath("//td[1]")).getText();
		assertEquals(data, dataAlterada);
	}

	@When("^eu clico no bot�o remover servi�o$")
	public void eu_clico_no_bot�o_remover_servi�o() throws Throwable {
	    driver.findElements(By.cssSelector(".combobox-clear")).get(0).click();
	}

	@When("^eu clico no bot�o remover cliente$")
	public void eu_clico_no_bot�o_remover_cliente() throws Throwable {
		driver.findElements(By.cssSelector(".combobox-clear")).get(1).click();
	}


}
