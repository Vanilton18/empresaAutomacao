package caboquinho.glue;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import caboquinho.utils.ScriptDatabase;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ClientSteps extends WebDriverCreator {

	@Given("^possuo o seguinte cliente registrado no sistema$")
	public void possuo_o_seguinte_cliente_registrado_no_sistema(DataTable table) throws Throwable {
		for (Map<String, String> linha : table.asMaps(String.class, String.class)) {
			String cliente = linha.get("Cliente");
			String email = linha.get("Email");
			String endereco = linha.get("Endere�o");
			String historico = linha.get("Hist�rico");
			ScriptDatabase.inserirClienteNaBase(cliente, email, endereco, historico);
		}
	}

	@When("^eu clico no menu \"(.*?)\"$")
	public void eu_clico_no_menu(String menuOpcao) throws Throwable {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("document.getElementById('" + menuOpcao + "').click();");
		// driver.findElement(By.id(menuOpcao)).click();
		assertNotNull(driver);
	}

	@Then("^eu estou na tela \"(.*?)\"$")
	public void eu_estou_na_tela(String telaCliente) throws Throwable {
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//html/body/div[2]/h1"), telaCliente));
		assertEquals(telaCliente, driver.findElement(By.cssSelector("h1")).getText());

	}

	@When("^eu clico no bot�o \"(.*?)\"$")
	public void eu_clico_no_bot_o(String btAdicionar) throws Throwable {
		action.moveToElement(driver.findElement(By.xpath("//button[contains(.,'" + btAdicionar + "')]"))).build().perform();
		action.click().build().perform();
		//driver.findElement(By.xpath("//button[contains(.,'" + btAdicionar + "')]")).click();
	}

	@Then("^eu estou no modal \"(.*?)\"$")
	public void eu_estou_no_modal(String tituloModal) throws Throwable {
		wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("h3")));
		assertEquals(tituloModal, driver.findElement(By.cssSelector("h3")).getText());
	}

	@When("^eu preencho os campos do modal Manter Cliente$")
	public void eu_preencho_os_campos_do_modal_Manter_Cliente(DataTable table) throws Throwable {
		for (Map<String, String> row : table.asMaps(String.class, String.class)) {
			driver.findElement(By.id("nome")).clear();
			driver.findElement(By.id("email")).clear();
			driver.findElement(By.id("endereco")).clear();
			driver.findElement(By.id("historico")).clear();
			driver.findElement(By.id("nome")).sendKeys(row.get("Nome"));
			driver.findElement(By.id("email")).sendKeys(row.get("Email"));
			driver.findElement(By.id("endereco")).sendKeys(row.get("Endere�o"));
			driver.findElement(By.id("historico")).sendKeys(row.get("Hist�rico"));
		}
	}

	@Then("^eu verifico a mensagem \"(.*?)\"$")
	public void eu_verifico_a_mensagem(String txMsgCadastro) throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//div[@class='alert alert-success']"),
				txMsgCadastro));
		assertTrue(
				driver.findElement(By.xpath("//div[@class='alert alert-success']")).getText().contains(txMsgCadastro));

	}

	@Given("^n�o possuo registros cadastrados no sistema$")
	public void n�o_possuo_registros_cadastrados_no_sistema() throws Throwable {
		ScriptDatabase.LimparBase();
	}

	@Given("^eu estou na tela de \"(.*?)\"$")
	public void eu_estou_na_tela_de(String txPaginaPrincipal) throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//html/body/div[3]/div/h1")));
		assertEquals(txPaginaPrincipal.toUpperCase(),
				driver.findElement(By.xpath("//html/body/div[3]/div/h1")).getText().toUpperCase());
	}

	@Given("^possuo o seguinte cliente cadastrado no sistema$")
	public void possuo_o_seguinte_cliente_cadastrado_no_sistema(DataTable table) throws Throwable {
		for (Map<String, String> linha : table.asMaps(String.class, String.class)) {
			String nome = linha.get("Nome");
			String email = linha.get("Email");
			String endereco = linha.get("Endere�o");
			String historico = linha.get("Hist�rico");
			ScriptDatabase.inserirClienteNaBase(nome, email, endereco, historico);
		}
	}

	@When("^eu clico no cliente \"(.*?)\"$")
	public void eu_clico_no_cliente(String cliente) throws Throwable {
		driver.findElement(By.xpath("//td[text()='" + cliente + "']")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("clienteModelContainer")));
	}

	@When("^eu clico no bot�o excluir cliente$")
	public void eu_clico_no_bot�o_excluir_cliente() throws Throwable {
		driver.findElement(By.id("bt_delete_cliente")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("bt_confirmar_excluir")));
	}

	@Then("^eu verifico o resultado na tabela de cliente$")
	public void eu_verifico_o_resultado_na_tabela_de_cliente(DataTable table) throws Throwable {
		String nome = driver.findElement(By.xpath("//td[1]")).getText();
		String email = driver.findElement(By.xpath("//td[2]")).getText();
		String endereco = driver.findElement(By.xpath("//td[3]")).getText();
		String historico = driver.findElement(By.xpath("//td[4]")).getText();

		for (Map<String, String> linha : table.asMaps(String.class, String.class)) {
			assertEquals(linha.get("Nome"), nome);
			assertEquals(linha.get("Email"), email);
			assertEquals(linha.get("Endere�o"), endereco);
			assertEquals(linha.get("Hist�rico"), historico);
		}
	}

}
